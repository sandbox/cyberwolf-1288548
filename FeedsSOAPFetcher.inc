<?php

/**
 * @file
 * Home of the FeedsSOAPFetcher and related classes.
 * Depends on soapclient module.
 *
 * Initial version by roderik (http://drupal.org/user/8841),
 * based on various files in Feeds distribution by alex_b
 * and SOAP Client module by ilo.
 *
 */

/**
 * Definition of the import batch object created on the fetching stage by
 * FeedsSoapFetcher.
 */
class FeedsSOAPBatch extends FeedsImportBatch {

  protected $config;

  protected $client;

  /**
   * Constructor.
   */
  public function __construct($config) {
    $this->config = $config;
    parent::__construct();
  }

  protected function getClient() {
    if (!isset($this->client)) {
      $options = array();

      if (!$this->config['wsdl']) {
        if (empty($this->config['namespace'])) {
          throw new Exception(t('Target namespace uri is required when using non-WSDL mode'));
        }

        $options['location'] = $this->config['endpoint'];
        $options['uri']      = $this->config['namespace'];

        // the style and use options only work in non-WSDL mode, in WSDL mode they come from the WSDL file
        $options['use'] = $this->config['use'] == 0 ? SOAP_ENCODED : SOAP_LITERAL;
        $options['style'] = $this->config['style'] == 0 ? SOAP_RPC : SOAP_DOCUMENT;
      }

      $options['features'] = SOAP_SINGLE_ELEMENT_ARRAYS; // check http://bugs.php.net/bug.php?id=36226 for more details

      // turn on tracing so we can use the full XML response to query with for example xpath
      $options['trace'] = TRUE;

      $this->client = new SoapClient(($this->config['wsdl'] ? $this->config['endpoint'] : NULL), $options);
    }

    return $this->client;
  }

  protected function getArgs() {
    // TODO rewrite this part of the code, as it just sucks now
    $param_str = preg_replace('/[\s]*=[\s]*/', '=', $this->config['arguments']);
    if (strlen($param_str) > 0) {
      $params = split("\n", $param_str);
    }
    else {
      $params = array();
    }

    $args = array();
    foreach ($params as $param) {
      list($name, $value) = split('=', $param);

      if (isset($value)) {
        $args[$name] = trim($value);
      }
      else {
        $args[] = trim($name);
      }
    }

    return $args;
  }

  /**
   * Implementation of FeedsImportBatch::getRaw().
   */
  public function getRaw() {
    $args = $this->getArgs();
    $client = $this->getClient();
    $client->__soapCall($this->config['function'], $args);

    $response = $client->__getLastResponse();

    return $response;
  }
}

/**
 * Fetches data via SOAP.
 */
class FeedsSOAPFetcher extends FeedsFetcher {

  /**
   * Implementation of FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    return new FeedsSOAPBatch($source_config);
  }

  /**
   * Source form.
   */
  public function sourceForm($source_config) {

    $form = array();

    $form['endpoint'] = array(
      '#type'  => 'textfield',
      '#title' => t('SOAP server endpoint URL'),
      '#size'  => 60,
      '#maxlength'   => 256,
      '#description' => t('Enter the absolute endpoint URL of the SOAP Server service. If WSDL is being used (see SOAPFetcher settings), this will be the URL to retrieve the WSDL.'),
      '#default_value' => isset($source_config['endpoint']) ? $source_config['endpoint'] : '',
      '#required'    => TRUE
    );

    $form['wsdl'] = array(
      '#type'  => 'checkbox',
      '#title' => t('Use WSDL?'),
      '#default_value' => $source_config['wsdl'],
    );

    $form['namespace'] = array(
      '#type'  => 'textfield',
      '#title' => t('Target Namespace'),
      '#default_value' => $source_config['namespace'],
      '#size'  => 60,
      '#maxlength'   => 256,
      '#description' => t('If WSDL is <strong>not</strong> used, enter the target namespace URI here. Otherwise, leave it blank.'),
    );

    $form['use'] = array(
      '#type'  => 'radios',
      '#title' => 'Use',
      '#default_value' => $source_config['use'],
      '#options'       => array('encoded', 'literal'),
      '#description'   => t('Specify how the SOAP client serializes the message.'),
    );

    $form['function'] = array(
      '#type'  => 'textfield',
      '#title' => t('SOAP Function'),
      '#size'  => 60,
      '#maxlength'   => 256,
      '#description' => t('Enter the function name to be called.'),
      '#default_value' => isset($source_config['function']) ? $source_config['function'] : '',
      '#required'    => TRUE
    );

    $form['arguments'] = array(
      '#type'  => 'textarea',
      '#title' => t('Arguments'),
      '#cols'  => 60,
      '#rows'  => 10,
      '#description' => t('Enter the arguments of the function. One argument per line, '.
                          'for named arguments, the format of <em>name=value</em> may be used.'),
      '#default_value' => isset($source_config['arguments']) ? $source_config['arguments'] : '',
    );

    return $form;
  }
}
